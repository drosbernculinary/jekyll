function goBack() {
    window.history.back();
}

/*
//only worked for the first instance of button on page
function goBackToTop() {
    const backToTopButton = document.getElementById("back-to-top-button");

    backToTopButton.addEventListener("click", () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth", // Add smooth scrolling animation
    });
});
}
*/

//go back to top for ALL BUTTONS
document.addEventListener("DOMContentLoaded", function () {
    function goBackToTop() {
        const backToTopButtons = document.querySelectorAll(".back-to-top-button");

        backToTopButtons.forEach(function (button) {
            button.addEventListener("click", () => {
                window.scrollTo({
                    top: 0,
                    behavior: "smooth", // Add smooth scrolling animation
                });
            });
        });
    }

    // Call the function to initialize it
    goBackToTop();
});

/*
//smooth scrolling to top (used on recipe posts)
document.addEventListener("DOMContentLoaded", function () {
    const backToTopButton = document.querySelector("a[href='#top']");

    backToTopButton.addEventListener("click", function (e) {
        e.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: "smooth", // Add smooth scrolling animation
        });
    });
});
*/

//smooth scrolling to anchors (used on food safety page)
document.addEventListener("DOMContentLoaded", function () {
    const scrollLinks = document.querySelectorAll("[data-scroll-to]");

    scrollLinks.forEach(function (link) {
        link.addEventListener("click", function (e) {
            e.preventDefault();
            const targetId = link.getAttribute("data-scroll-to");
            const targetElement = document.getElementById(targetId);

            if (targetElement) {
                window.scrollTo({
                    top: targetElement.offsetTop,
                    behavior: "smooth",
                });
            }
        });
    });
});

/*
//hero section
*/
document.addEventListener("DOMContentLoaded", function () {
    const homePageBackground = document.getElementById("home-page-background");
    homePageBackground.style.backgroundColor = `rgb(0, 0, 0)`;
});

window.addEventListener("scroll", function () {
    const scrollPosition = window.scrollY;
    const homePageBackground = document.getElementById("home-page-background");
    const heroImage = document.getElementById("heroImage");
    const maxScroll = 500; // Set the scroll position where the transition completes

    // Calculate a scroll percentage based on the current scroll position and the maximum scroll position
    const scrollPercentage = Math.min(scrollPosition / maxScroll, 1);

    // Calculate the background color based on the scroll percentage
    const backgroundColor = `rgb(${scrollPercentage * 255}, ${scrollPercentage * 255}, ${scrollPercentage * 255})`;

    // Apply the calculated background color to the homePageBackground element
    homePageBackground.style.backgroundColor = backgroundColor;

    // Toggle the visibility of the hero image based on the scroll position
    heroImage.style.display = scrollPosition < maxScroll ? "block" : "none";
});