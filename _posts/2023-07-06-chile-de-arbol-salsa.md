---
layout: post
title: "Chile de Arbol Salsa"
date: 2023-07-06 09:00:59 -0700
categories:
 - mexican
 - salsas
info: "Very spicy salsa."
image: /images/chile_de_arbol_salsa.jpg
ingredients:
  - 1/2 bag (about 1 to 1 1/2 oz) Chile de Arbol Tostado
  - 1 tbsp Consume de Pollo
  - 1 Garlic Clove
  - Water
  - 1 - 29 oz can Tomato Sauce
  - Salt
  - 1/2 Onion, diced
  - Cilantro, chopped
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Remove stems from chiles and place in a blender.</li>
  <li>Add consume de pollo and garlic.</li>
  <li>Add enough water to blend the chiles.</li>
  <li>Blend until smooth.</li>
  <li>Add tomato sauce and salt to taste.</li>
  <li>Blend again.</li>
  <li>Pour salsa into a bowl and mix in the onion and cilantro.</li>
</ol>