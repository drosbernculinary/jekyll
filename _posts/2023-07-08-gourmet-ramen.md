---
layout: post
title: "Gourmet Ramen"
date: 2023-07-08 09:00:59 -0700
categories:
 - mains
 - asian
 - soups
info: "Rich and aromatic broth, fresh vegetables, soft cooked egg. This is the only way to eat ramen."
image: /images/gourmet_ramen.jpg
ingredients:
  - 1/2 cup Onions, 1/4" sliced
  - 1/2 cup Bell Pepper, 1/4" sliced
  - 1/2 cup Mushrooms, 1/4" sliced
  - 1/2 cup Protien of Choice (steak, pork, or chicken (thinly sliced), shrimp, or scallops)
  - 1 tbsp Garlic, chopped
  - 1 tbsp Ginger, minced
  - 2 1/3 cup Water
  - 1 pkg Shin Ramyun Black Or Shin Ramyun Red Instant Noodles
  - 1/4 cup (2 oz) Teriyaki Sauce (Optional)
  - 2 cups Spinach, destemmed
  - 1 Large Egg
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Over medium heat use some butter or oil to begin cooking onion, bell pepper, and mushroom.</li>
  <li>When the vegetables are mostly softened, add your protein and cook through.</li>
  <li>Add garlic and ginger and continue cooking for about 30 seconds or until aromatic.</li>
  <li>When you can smell the garlic and ginger, immediately add the water.</li>
  <li>Add all the seasoning packets from the package of ramen noodles. (For Shin Ramyun Black, there will be three packets. One soup base, one chili mix, and one dehydrated vegetable packet.)</li>
  <li>If using teriyaki sauce, add it now</li>
  <li>Next, add the spinach and everything together.</li>
  <li>Bring the mixture to a low boil</li>
  <li>Break the ramen noodles into 4 pieces (this allows room to place the egg).</li>
  <li>Put the dry ramen noodles into the pot and use a spoon to make sure they are all covered with liquid</li>
  <li>Gently crack the egg into a space between the chunks of noodles and cook at a low boil for 4 minutes.</li>
  <li>Remove from heat and allow to stand for a few minutes. The egg will continue to cook during this time</li>
  <li>When ready to serve, gently remove the egg with a spoon and set aside. Transfer the ramen and broth to a bowl and place the egg on top.</li>
</ol>