---
layout: post-carousel
title: "Lemonade and Limeade"
date: 2023-09-02 09:00:59 -0700
categories:
 - beverages
info: "A refreshing drink with the perfect balance of sweet and sour."
image: /images/lemonade_and_limeade_1.jpg
carousels:
  - images: 
    - image: /jekyll/images/lemonade_and_limeade_1.jpg
    - image: /jekyll/images/lemonade_and_limeade_2.jpg
    - image: /jekyll/images/lemonade_and_limeade_3.jpg
    - image: /jekyll/images/lemonade_and_limeade_4.jpg
    - image: /jekyll/images/lemonade_and_limeade_5.jpg
ingredients:
  - 1 cup Freshly Squeezed Lemon Juice or Lime Juice
  - 1 cup Granulated Sugar
  - 5 cups Cold Water
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Mix together the freshly squeezed lime juice and sugar in a pitcher.</li>
  <li>Add cold water and mix.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>The ratio is 1 part juice, 1 part sugar, and 5 parts water. You can easy scale this by measuring how much juice you have, adding an equal amount of sugar, and then multiply the amount of juice by 5 to calculate the amount of water. For example, 2/3 cup of juice + 2/3 cup sugar + 3 1/3 cups of water.</li>
</ul>