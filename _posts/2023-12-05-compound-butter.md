---
layout: post
title: "Compound Butter"
date: 2023-12-5 09:00:59 -0700
categories:
 - sauces-and-condiments
 - seasonings-and-marinades
info: "Essentially, seasoned butter."
image: /images/image_coming_soon.jpg
ingredients:
  - 1 stick (4 oz or 1/2 cup) Salted Butter
  - 1/4 cup Honey
  - 1/2 tsp Ground Cinnamon
  - 1/4 tsp Vanilla Extract
ingredients2:
  - 1 stick (4 oz or 1/2 cup) Salted Butter
  - 1 1/2 tsp Granulated Garlic
  - 1/2 tsp Onion Powder
  - 1/4 tsp Black Pepper
  - 1 tsp Dried Thyme
  - 1 tsp Dried Sage
  - 1 tsp Dried Rosemary
---

<p>{{ info }}</p>

<h2>Cinnamon Honey Butter</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Savory Herb Butter for Basting Turkey</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Allow butter to come to room temperature.</li>
  <li>Place all ingredients in a mixing bowl and stir until well combined.</li>
  <li>Lay out a layer of plastic wrap on your work surface.</li>
  <li>Scoop compound butter into a horizontal line.</li>
  <li>Fold one side of the plastic wrap over the line of butter and squeeze gently to remove any air.</li>
  <li>Grab the ends of the plastic just past each end of the butter. Roll forwards to tighten the plastic around the butter to create a log.</li>
  <li>Refrigerate or freeze until ready to use.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Ideas from Thermoworks <a href="https://blog.thermoworks.com/sides/compound-butter/">here</a></li>
  <li>Red wine reduction, parsley, shallot, and lemon. Great with red meat and pork.</li>
  <li>Use a small saucepan over low heat to simmer the port (I’d say maybe a cup for starters) until it becomes thick and almost syrupy. Cool, then beat it into the butter in a slow stream in a stand mixer.</li>
  <li>Tarragon and orange zest. Great with finfish and shellfish.</li>
  <li>Port reduction. Great with beef and lamb.</li>
  <li>Shallot, parsley and garlic. Perfect for escargot.</li>
  <li>Gorgonzola and chives. Great with red meat.</li>
  <li>Vanilla bean and honey. Great with breads and pancakes.</li>
  <li>Honey lavender. Great with breads and pancakes.</li>
  <li>Dill and lemon zest. Great with salmon.</li>
  <li>Curry paste and cilantro. Great for cucumber tea sandwiches.</li>
  <li>Chives and dijon mustard. Great with steamed veggies and pork.</li>
  <li>Roasted red pepper (drained well). Great with grilled chicken.</li>
  <li>Fig preserves (or any fruit preserve). Great with breads and pancakes.</li>
  <li>Anchovies, garlic, and lemon zest. Great with grilled steak and sautéed vegetables.</li>
  <li>Parsley, sage, rosemary, and thyme. Great with poultry (especially a Thanksgiving turkey!).</li>
  <li>Cracked pink, black, or green peppercorns, thyme, lemon juice, and garlic. Great with beef and lamb.</li>
  <li>Brown sugar, cinnamon, nutmeg, and chopped walnuts. Great with breads and pancakes.</li>
</ul>