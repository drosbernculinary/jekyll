---
layout: post
title: "Restaurant Style Salsa"
date: 2023-07-09 09:00:59 -0700
categories:
 - mexican
 - salsas
info: "Canned tomatoes ensure this salsa tastes great any time of the year."
image: /images/restaurant_style_salsa.jpg
ingredients:
  - 14 oz can Fire Roasted Crushed Tomatoes
  - 1/2 Red Onion, minced
  - 6 cloves Garlic, minced
  - 30 sprigs Cilantro, minced
  - 1 Lime, juiced
  - 2 Jalapenos, finely diced
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients.</li>
  <li>Store refrigerated overnight for flavors to meld.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>The salsa will be very stong on onion flavor right when it's made, but the next day it will mellow out a lot and taste much better.</li>
</ul>