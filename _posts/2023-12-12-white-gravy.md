---
layout: post
title: "White Gravy"
date: 2023-12-12 09:00:59 -0700
categories:
 - sauces-and-condiments
 - breakfast
info: "Creamy gravy that is perfect on country fried steak and mashed potatoes. Or add sausage and serve over biscuits."
image: /images/white_gravy.jpg
ingredients:
  - 1/4 cup Butter
  - 1/4 cup All-purpose Flour
  - 2 cups Whole Milk
  - 1/4 tsp Dried Thyme
  - 1/4 tsp Cayenne
  - 1 tsp Salt
  - Black Pepper
  - 1/2 lb Ground Breakfast Sausage, cooked (optional)
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Melt butter in a saucepan. Add flour and stir together. Cook for 3 minutes, stirring often.</li>
  <li>Add the milk, thyme, cayenne, salt and pepper (to taste) and whisk together.</li>
  <li>Continue cooking over medium-low heat, stirring often, until thick.</li>
  <li>For sausage gravy: stir in cooked sausage.</li>
</ol>