---
layout: post
title: "Garlic Cheese Biscuits"
date: 2023-10-25 09:00:59 -0700
categories:
 - appetizers
 - sides
 - breads
 - restaurant-recipes
info: "These delicious biscuits are ready anytime. All you need is cheddar cheese, butter, and water."
image: /images/image_coming_soon.jpg
ingredients:
  - 3 3/4 cup Biscuit Mix (Bisquick)
  - 2 tbsp Granulated Garlic
  - 1 tsp Onion Powder
  - 2 tbsp Dried Parsley
  - 1 1/2 tsp Kosher Salt
ingredients2:
  - 1 cup Grated Sharp Cheddar Cheese
  - 1/2 lb (1 stick) Butter, melted
  - 1 cup Water
---

<p>{{ info }}</p>

<h2>Dry Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Fresh Ingredients</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine biscuit mix, granulated garlic, onion powder, dried parsley, and kosher salt in a bowl.</li>
  <li>Optional: Prep ahead and set aside combined dry ingredients for easy cooking later.</li>
  <li>Prepare a greased baking sheet, and preheat oven to 350 degrees F.</li>
  <li>Add grated sharp cheddar cheese, melted butter, and water to the dry ingredients. Mix gently to incorporate but do not over mix. Dough should still have some lumps left in it.</li>
  <li>Use a #20 scoop (2 1/2 oz) to portion dough onto the greased baking sheet.</li>
  <li>Bake at 350 degrees F for 13-15 minutes, rotating once halfway through.</li>
  <li>Optional: Brush with melted butter or garlic butter after removing from oven.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Yields approximately 12 biscuits.</li>
</ul>