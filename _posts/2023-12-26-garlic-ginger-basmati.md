---
layout: post
title: "Garlic Ginger Basmati (Rice Cooker Version)"
date: 2023-12-26 09:00:59 -0700
categories:
 - sides
 - asian
info: "A savory side dish with a touch of sweetness from the aromatic spices. The nuttiness from the basmati rice is balanced with the pungency of fresh garlic and the zesty warmth of ginger."
image: /images/garlic_ginger_basmati.jpg
ingredients:
  - 1 cup Basmati Rice
  - 1 3/4 cups Chicken Stock
  - 1 tbsp Chopped Garlic
  - 1 tbsp Pureed Ginger
  - 1 tbsp Melted Butter
---

<p>{{ info }}</p>

<p>This rice is versatile and pairs well with a variety of dishes, especially those with Asian or Indian influences. It can be enjoyed on its own or as a side dish to complement curries, stir-fries, grilled meats, or vegetables.</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place rice, chicken stock, garlic, ginger, and butter into the rice cooker.</li>
  <li>Power the unit on and select rice/grains setting.</li>
  <li>Wait until the cooking cycle finishes (about 40 minutes).</li>
  <li>Let the rice rest for 20 minutes.</li>
  <li>Open the unit and gently fluff the rice.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Need to test if the rice needs to rest after cooking, but this method resulted in pefectly cooked basmati rice.</li>
  <li>After 40 minutes the unit appeared to be completely off. What happened to the keep warm feature?</li>
  <li>Need to experiment with adding black pepper, onions, onion powder, or sugar/honey.</li>
  <li>Need to experiement with toasting the rice.</li>
</ul>