---
layout: post
title: "Smokehouse BBQ Cheesy Corn - Copycat"
date: 2023-08-19 09:00:59 -0700
categories:
 - sides
 - slow-cooker
 - restaurant-recipes
info: "Cheesy and creamy corn side dish served by all the mainstream BBQ restaurants."
image: /images/cheesy_corn.jpg
ingredients:
  - 1 stick (8 tbsp or 1/2 cup) Butter, cut into small cubes
  - 8 oz Cream Cheese, cut into small cubes
  - 3/4 cup Whole Milk
  - 1 1/2 cups Shredded Cheddar Cheese
  - 1/4 lb Thin Sliced Deli Ham, cut into small cubes
  - 1/2 ea Red Bell Pepper, minced (seeds and pith removed)
  - 1/2 tsp Salt
  - 1/2 tsp Pepper
  - 1 1/2 tsp Granulated Garlic
  - 58 oz (2 big - 29 oz cans) Cut Corn
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Heat a 3 qt slow cooker on high and combine butter, cream cheese, milk, shredded cheddar cheese, sliced ham, red pepper, salt, pepper and garlic. Stir until mixture begins to melt.</li>
  <li>Open and drain cans of corn, then add them to the crockpot.</li>
  <li>Cook, covered, on high until heated through and cheese is melted (about 1 ½ - 2 hours), stirring occasionally.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This recipe calls for canned corn but frozen can also be used.</li>
  <li>Be careful about leaving the cheesy corn in the crockpot for too long. The high heat can make the cream cheese grainy after a while.</li>
</ul>