---
layout: post
title: "Garlic and Chive Mashed Potatoes"
date: 2023-12-07 09:00:59 -0700
categories:
 - sides
info: "Delicious both with and without gravy."
image: /images/garlic_and_chive_mashed_potatoes.jpg
ingredients:
  - 5 lbs Russet Potatoes, peeled and chopped
  - 2 cups Sour Cream (or to taste)
  - 3 tsp Salt (or to taste)
  - 3 tsp Granulated Garlic (or 5 tsp puree)
  - 1 1/2 tsp Black Pepper
  - 8 oz (2 sticks) Salted Butter
  - 1 - 0.8 oz pkg Fresh Chives, sliced
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place potatoes into a large pot and fill with enough water to cover.</li>
  <li>Bring to a boil and continue cooking until potatoes are fork tender.</li>
  <li>Drain water and stir in remaining ingredients.</li>
</ol>