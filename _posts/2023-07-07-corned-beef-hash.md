---
layout: post
title: "Corned Beef Hash"
date: 2023-07-03 09:00:59 -0700
categories:
 - mains
 - beef
 - breakfast
info: "Corned beef cooked with garlic and onion until tender and crispy. Perfect with over-easy eggs and toast."
image: /images/corned_beef_hash.jpg
ingredients:
  - Butter
  - Corned Beef, cooked, diced 1/4"
  - Potatoes cooked, cooled, diced 1/4"
  - Onion, diced 1/4"
  - Granulated Garlic
  - Seasoned Salt
  - Black Pepper
  - Tobasco sauce
  - Garlic Powder
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Melt butter in a heated skillet.</li>
  <li>Add corned beef, potatoes, and onions.</li>
  <li>Season with granulated garlic, seasoned salt, and black pepper.</li>
  <li>Cook until the corned beef is crispy, and the potatoes and onions are golden brown.</li>
  <li>Serve with over-easy eggs and toast.</li>
</ol>