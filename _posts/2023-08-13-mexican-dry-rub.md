---
layout: post
title: "Mexican Dry Rub"
date: 2023-08-13 09:00:59 -0700
categories:
 - seasonings-and-marinades
 - mexican
 - bbq-and-grilling
info: "Dry rub that brings Mexican flavors to smoking or grilling."
image: /images/mexican_dry_rub.jpg
ingredients:
 - 2 tablespoons chile powder
 - 1 tablespoon garlic powder
 - 1/2 tablespoon onion powder
 - 1 tablespoon dried oregano
 - 1 tablespoon paprika
 - 1 1/2 tablespoon ground cumin
 - 1/2 tablespoon sea salt
 - 1 tablespoon black pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients in a small bowl.</li>
  <li>The seasoning mixture is ready to use immediately or can be stored in an air-tight container for later use.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>I use this rub when I smoke meat for use in a mexican themed dish.</li>
</ul>