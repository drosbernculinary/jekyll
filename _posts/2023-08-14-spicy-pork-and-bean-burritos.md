---
layout: post
title: "Spicy Pork and Bean Burritos"
date: 2023-08-14 09:00:59 -0700
categories:
 - mains
 - pork
 - mexican
info: "So easy to make. Mix up the filling, roll it up in a tortilla, and drown in salsa!"
image: /images/spicy_pork_and_bean_burritos.jpg
ingredients:
 - Approximately 1 pound Shredded Pork
 - 2 - 16 oz cans Refried Beans
 - 1 - 16 oz can Pace Brand Salsa “Hot”
 - 1 - 2 cup bag Shredded Sharp Cheddar Cheese
 - ½ of a 12 oz can Sliced jalapeno in a jar, diced small
 - 10” Flour tortillas
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place pork, beans, salsa, cheese, and jalapenos in a bowl and fold gently until combined.</li>
  <li>Warm tortillas and place 3 #20 (2 1/2 oz) scoops into the center of each tortilla.</li>
  <li>Roll burritos and place seam side down into a dry skillet over medium to medium-high heat.</li>
  <li>Cook for 30 - 60 seconds or until nicely browned, then turn and repeat on the other side.</li>
  <li>Finish cooking in the oven or microwave until heated through. Serve with extra hot sauce</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Chicken can also be used in this recipe instead of the pork.</li>
</ul>