---
layout: post
title: "Smith & Wollensky's Cajun Steak Seasoning"
date: 2023-08-13 09:00:59 -0700
categories:
 - seasonings-and-marinades
 - beef
 - bbq-and-grilling
 - restaurant-recipes
info: "A delicious blend of spices that adds a nice Cajun flavor without overwhelming the steak. Also great on burgers."
image: /images/cajun_steak_seasoning.jpg
ingredients:
 - 1 tbsp cayenne pepper
 - 1 tbsp chili powder
 - 1 tbsp garlic powder
 - 1 tbsp ground black pepper
 - 1 tbsp ground white pepper
 - 1 tbsp onion powder
 - 1 tbsp smoked paprika
 - 1 ½ tsp ground cumin
 - 1 tsp dried basil
 - 1 tsp dried oregano
 - 1 tsp dried thyme
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients in a small bowl.</li>
  <li>Place spice mixture into a dry skillet and cook over medium heat, stirring frequently, until fragrant, about 3 minutes.</li>
  <li>Remove the spice mixture from the skillet to cool.</li>
  <li>The seasoning mixture is ready to use immediately or can be stored in an air-tight container for later use.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This is awesome on both steaks and burgers.</li>
</ul>