---
layout: post
title: "Battered Fish"
date: 2023-07-08 09:00:59 -0700
categories:
 - mains
 - seafood
info: "Flaky and moist with a crispy golden crust."
image: /images/battered_fish.jpg
ingredients:
  - 3/4 cup Flour
  - 1/2 tsp Salt
  - 1/2 tsp Black Pepper
  - 3/4 tsp Old Bay Seasoning
  - 1/2 tsp Paprika
  - 1/2 tsp Baking Powder
  - Carbonated Water
  - Fish
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine the flour, salt, pepper, Old Bay seasoning, paprika, and baking powder.</li>
  <li>Add just enough carbonated water to the dry ingredients to form a batter that is slightly less thick than pancake batter.</li>
  <li>Dry the fish, coat in flour, and then dip into the batter.</li>
  <li>Fry for 3 to 3 1/2 minutes or until golden brown.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This recipe barely made enough to make 7 pcs of fish.</li>
  <li>Dipping the fish partway into the oil and waiting about 5 seconds before letting go allows the batter to set up and begin to rise, helping to prevent the fish from sticking to the fry basket.</li>
  <li>For beer battered fish, replace the carbonated water with beer.</li>
</ul>