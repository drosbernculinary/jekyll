---
layout: post-carousel
title: "Mexican Street Style Corn"
date: 2023-09-04 09:00:59 -0700
categories:
 - sides
 - mexican
info: "Roasted corn in a creamy sauce with spices and crumbled cheese."
image: /images/mexican_street_corn_1.jpg
carousels:
  - images: 
    - image: /images/mexican_street_corn_1.jpg
    - image: /images/mexican_street_corn_2.jpg
    - image: /images/mexican_street_corn_3.jpg
    - image: /images/mexican_street_corn_4.jpg
ingredients:
  - 2 tbsp Butter
  - 3 (14.5 oz) cans Cut Corn (drained)
  - 1/4 cup Red Bell Pepper, 1/4" diced (optional)
  - Salt
  - 3 tbsp Mayonnaise
  - Juice from 1 Lime
  - 1 tsp Chili Powder (plus more for garnishing)
  - Cotija or Feta Cheese
  - Chopped Cilantro
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Melt butter over medium-high heat.</li>
  <li>Add the corn (and red pepper, if using) and cook for 8-10 minutes, stirring occasionally.</li>
  <li>Place corn in a bowl and add mayonnaise, lime juice, and chili powder and mix well.</li>
  <li>Pour corn into a serving dish and sprinkle cheese, cilantro and chili powder on top.</li>
  <li>Serve immediately.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Adapted from: <a href="https://lilluna.com/mexican-street-corn/" target="_blank" rel="noopener noreferrer">Lil Luna's Mexican Street Corn</a></li>
</ul>