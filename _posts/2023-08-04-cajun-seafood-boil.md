---
layout: post
title: "Cajun Seafood Boil"
date: 2023-08-04 09:00:59 -0700
categories:
 - mains
 - seafood
info: "Shrimp, andouille sausage, and any other seafood boiled with aromatics in a spicy cajun broth."
image: /images/cajun_seafood_boil.jpg
ingredients:
  - 6 qts water
  - 1 oz per lb of seafood Zatarain's Pro Boil
  - 1 Garlic Head, whole, sliced in half
  - 1 Yellow Onion, whole, sliced in half
  - 6 Celery Ribs, sliced into 3-inch-long sections
  - 1 Lemon, whole, sliced in half
  - 3-4 Red Potatoes, sliced in half
  - 8 ea Corn on the Cob, cleaned and cut in half
  - Crab Legs, Clams, Lobster Tails, and/or any other seafood (optional)
  - 2 packages Andouille or Smoked Sausage, cut into 2-inch-long sections
  - 4 lbs Shrimp, shell-on/tail-on/deveined, 16-20 size or larger
  - 1 lb Mushrooms
  - Ice
  - 3 tbsp Butter
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place 6 quarts of water in a 12-quart pot. Add oil seasoning and mix well.</li>
    <li>Add garlic, onion, and celery. Squeeze the juice from the lemons into the pot, then throw the lemon peels in the pot too.</li>
    <li>Once the pot is at a full boil, mix it around, then add the potatoes. Boil for 10-12 minutes.</li>
    <li>At this point add other ingredients and optional seafood. They items will cook at different rates. Put the items that takes the longest amount of time to cook, first, then add the next, and so on.</li>
    <ul>
        <li>Lobster Tail: 8-12 minutes</li>
        <li>Corn: 8 minutes</li>
        <li>Clams: 5-10 minutes</li>
        <li>Crab Legs: 6-8 minutes for pre-cooked (20 minutes for raw crab)</li>
        <li>Shrimp: 2-3 minutes</li>
        <li>Sausage: 2 minutes</li>
    </ul>
    <li>When everything is cooked, reserve 1 cup of cooking liquid.</li>
    <li>Add ice to pot and mix to stop the boil. Allow the seafood mixture to soak for 5-10 minutes.</li>
    <li>Mix butter and reserved cooking liquid together until butter is melted and combined.</li>
    <li>Drain the cooking liquid from the seafood mixture and place back into the empty pot or a large mixing bowl.</li>
    <li>Pour butter mixture over seafood mixture and mix.</li>
    <li>Serve.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This recipe requires a very large pot.</li>
</ul>