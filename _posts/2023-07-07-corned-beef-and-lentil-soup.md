---
layout: post
title: "Corned Beef and Lentil Soup"
date: 2023-07-03 09:00:59 -0700
categories:
 - mains
 - beef
 - soups
info: "Best served with some crusty bread to soak up all the flavor."
image: /images/corned_beef_and_lentil_soup.jpg
ingredients:
  - Oil
  - 1 Yellow Onion, diced
  - 3 Carrots, diced
  - 3 ribs Celery, diced
  - 6 tbsp Garlic
  - 1 Bay Leaf
  - 1 ½ tsp	Dried Thyme
  - 9 cups Water
  - 3 tbsp Chicken Base
  - 1 – 14 oz can Canned Diced Tomatoes
  - 2 – 2 ½ cups (1 lb bag) Dry Lentils
  - 1 ½ - 2 lbs	Corned Beef, pulled into chunks
  - Spinach, destemmed and cut into strips
  - 2 tbsp Vinegar
  - (about 3 ½ tsp or to taste) Salt
  - Black Pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Heat oil in a large pot over medium heat. Add onions, carrots, and celery; cook and stir until softened.</li>
  <li>Stir in garlic, bay leaf, and thyme; cook until garlic is fragrant.</li>
  <li>Add water, chicken base, tomatoes, and lentils. Bring to a boil.</li>
  <li>Add in corned beef.</li>
  <li>Reduce heat and simmer until lentils are tender.</li>
  <li>Stir in spinach and cook until it wilts.</li>
  <li>Stir in vinegar and season with salt and pepper.</li>
</ol>