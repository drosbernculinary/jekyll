---
layout: post
title: "Mexican Rice"
date: 2023-09-03 09:00:59 -0700
categories:
 - sides
 - mexican
info: "One of the classic accompaniments to all Mexican cuisine. Long grain rice simmered with aromatics and a tomato based broth, giving it a reddish-orange hue."
image: /images/mexican_rice.jpg
ingredients:
  - 2 cups Long Grain White Rice
  - Oil
  - 4 cups Chicken Stock
  - 4 tsp Minced Garlic
  - 12 oz Tomato Sauce
  - 6 Stems of Cilantro (optional)
  - 1/4 tsp Cumin
  - 1/2 tsp Black Pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place rice and oil in a medium saucepan. Cook, stirring often, until golden brown.</li>
  <li>Add the garlic and stir until aromatic (about 30 seconds). Then add the stock to stop the garlic from cooking too long.</li>
  <li>Add tomato sauce, cilantro (if using), cumin and black pepper.</li>
  <li>Stir together and bring to a boil.</li>
  <li>Cover the pan and reduce heat to very low. Let the rice simmer for 12-15 minutes, then remove from heat and allow to stand for 10 minutes to finish cooking.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Basmati rice works great for this recipe.</li>
  <li>Adapted from: <a href="https://lilluna.com/food-tutorial-spanish-rice/" target="_blank" rel="noopener noreferrer">Lil Luna's Homemade Spanish Rice</a></li>
</ul>