---
layout: post
title: "Collard Greens"
date: 2023-08-06 09:00:59 -0700
categories:
 - sides
 - pork
info: "A classic southern side dish. Greens braised in a rich smokey sauce with plenty of bacon and onions."
image: /images/collard_greens.jpg
ingredients:
  - 1/2 cup Bacon, diced
  - 1/2 cup Onions, diced
  - 6 cups Water
  - 1 tbsp White Vinegar
  - 2 tbsp Ham Base
  - 1/2 tbsp Tabasco Sauce
  - 1 tbsp Worcestershire Sauce
  - 1 tbsp Molasses
  - 1/8 tsp Dry Mustard
  - 1/4 tsp Garlic Powder
  - 1/2 tsp Black Pepper
  - 1/2 cup Cabbage Seasoning (optional)*
  - 1 gallon Fresh Collard Greens, stems removed and chopped into 1"x1" pieces
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>In a large pot cook bacon until just until it begins to render some fat.</li>
  <li>Add onions and continue cooking until the bacon is fully rendered and the onions have gotten some good color.</li>
  <li>Add water, vinegar, ham base, Tabasco sauce, Worcestershire, molasses, dry mustard, garlic powder, black pepper, and cabbage seasoning (if using).</li>
  <li>Mix all the ingredients together, then add the collard greens.</li>
  <li>Bring to a boil, then reduce heat and simmer for about 1 hour or until the greens are tender and the sauce has reduced and thickened a bit.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Cabbage seasoning is optional and may be removed from the recipe in the future.</li>
</ul>