---
layout: post
title: "Sports Bar/Chicken Shop Fry Seasoning"
date: 2023-08-17 09:00:59 -0700
categories:
 - seasonings-and-marinades
 - restaurant-recipes
info: "Smoky and earthy. Perfect for sprinkling onto crispy fries as they come out of the oil or for dusting fried chicken strips."
image: /images/fry_seasoning.jpg
ingredients:
  - 2 tbsp Chili Powder
  - 2 tbsp Paprika
  - 2 tbsp Salt
  - 2 tbsp Cumin
  - 1 tbsp Garlic Powder
  - 1 tbsp Onion Powder
  - 1 tbsp Oregano
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Mix all ingredients together.</li>
  <li>Store in a cool dry place until ready to use.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Use to season french fries or to dust chicken strips before serving.</li>
</ul>