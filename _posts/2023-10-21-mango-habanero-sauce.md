---
layout: post
title: "Mango Habanero Sauce"
date: 2023-10-20 09:00:59 -0700
categories:
 - appetizers
 - sauces-and-condiments
 - chicken
info: "Fiery, fruity, and sweet glaze for wings or tenders."
image: /images/mango_habanero_sauce.jpg
ingredients:
  - 1 cup Mango Nectar
  - 1 1/2 tsp Habanero Tobasco
  - 2 tsp Sugar
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Mix mango nectar, habanero tobasco, and sugar together in a saucepan.</li>
  <li>Bring to a simmer over medium heat and reduce for 10-15 minutes or until thick.</li>
  <li>Toss wings in sauce or serve as a dipping sauce.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>1 tbsp of habanero Tobasco per cup of mango is very spicy.</li>
  <li>Adapted from u/mawcopolow's recipe <a href="https://www.reddit.com/r/food/comments/ujrzo2/homemade_mango_habanero_wings/">here</a>.</li>
</ul>