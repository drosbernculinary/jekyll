---
layout: post
title: "Stir Fry Sauce"
date: 2023-11-02 09:00:59 -0700
categories:
 - sauces-and-condiments
 - asian
info: "General purpose sauce that can be made ahead and kept in the fridge for making quick stir fry dishes."
image: /images/stir_fry_sauce_1.jpg
ingredients:
  - 2 tbsp Cooking Oil
  - 2 tbsp Garlic, minced
  - 2 tbsp Ginger, pureed
  - 1/2 cup Chicken Stock
  - 1/4 cup Oyster Sauce
  - 1/4 cup Rice Vinegar
  - 1/3 cup Light Soy Sauce
  - 1/3 cup Shaoxing Wine
  - 1/4 tsp Sesame Oil
  - 2 tbsp Sugar
  - 1/2 tsp White Pepper
  - 1 tbsp Cornstarch
  - 2 tbsp Water
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place chicken stock, oyster sauce, rice vinegar, light soy sauce, shaoxing wine, sesame oil, sugar, and white pepper into a bowl. Mix until all the ingredients are combined and the sugar is dissolved.</li>
  <li>In a separate bowl, create a cornstarch slurry by mixing cornstarch and cold water. Set aside.</li>
  <li>Heat oil in a saucepan over medium heat until shimmering.</li>
  <li>Stir in garlic and ginger and cook until fragrant, about 20-30 seconds.</li>
  <li>Add ingredient mixture from above, stir, and bring to a boil. Boil for one minute.</li>
  <li>Add cornstarch slurry, mix well, and remove from heat.</li>
  <li>Allow sauce to cool, then place into a sealed container, and store under refrigeration.</li>
  <li>To use: after stir frying your aromatics/vegetables/proteins, add a few tablespoons of stir fry sauce. Mix together and cook until it reduces a little and serve.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Adapted from HAPPY WOK on YouTube <a href="https://www.youtube.com/watch?v=zEHA1OBwXFQ">here</a>.</li>
  <li>The 1/4 tsp of sesame oil is not noticable in the finished version of the sauce.</li>
</ul>