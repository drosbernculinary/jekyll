---
layout: post
title: "Chicken And Dumplings"
date: 2023-07-06 09:00:59 -0700
categories:
 - mains
 - chicken
info: "Tender dumplings in a thick savory sauce of rosemary and thyme."
image: /images/chicken_and_dumplings.jpg
ingredients:
  - Cooking Oil
  - 1 1/2 lbs Chicken Thighs, boneless/skinless, cut into 1" cubes
  - Salt and Pepper
ingredients2:
  - Cooking Oil
  - 1 1/2 cups Onion, 1/4" diced
  - 1 1/2 cups Carrots, 1/4" diced
  - 1 1/2 cups Celery, 1/4" diced
  - 4 tbsp Garlic, chopped
  - 1/2 cup Butter
  - 1/2 cup Flour
  - 4 tsp Chicken Base
  - One 12 oz can Evaporated Milk
  - Chicken (from above)
  - 3/4 tsp Salt
  - 2 tsp Black Pepper
  - 1 tsp Dried Thyme
  - 1 tsp Dried Rosemary
ingredients3:
  - 2 cups All Purpose Flour
  - 1 tbsp + 1 tsp Baking Powder
  - 1 tsp Salt
  - 1 tsp Black Pepper
  - 1 tsp Dried Thyme
  - 1 tsp Dried Rosemary
  - 3/4 cup (+ a few splashes) Whole Milk
  - 4 tbsp Butter, melted
---

<p>{{ info }}</p>

<h2>Chicken Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Soup Ingredients</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
  </ul>

<h2>Dumpling Ingredients</h2>

<ul>
    {% for ingredient3 in page.ingredients2 %}
      <li>{{ ingredient3 }}</li>
    {% endfor %}
  </ul>

<h2>Chicken Instructions</h2>

<ol>
  <li>Prepare the chicken by removing any excess fat, cutting into cubes, and seasoning with salt and pepper.</li>
  <li>Heat some oil in a wide pot that has a lid; then cook chicken pieces until lightly browned and cooked through.</li>
  <li>Remove cooked chicken from the pan to rest while making the soup.</li>
</ol>

<h2>Soup Directions</h2>

<ol>
  <li>Heat some oil in the pot.</li>
  <li>Add onion, carrots, and celery. Cook for several minutes, until vegetables are soft.</li>
  <li>Add garlic and cook until fragrant.</li>
  <li>Add butter and flour, then stir to combine. Cook for 1 minute.</li>
  <li>Add hot water and chicken base. Stir until chicken base is dissolved.</li>
  <li>Add evaporated milk and stir together.</li>
  <li>Add salt, black pepper, thyme, and rosemary, then bring to a boil.</li>
  <li>Reduce heat and allow to simmer while proceeding to making the dumplings.</li>
</ol>

<h2>Dumpling Directions</h2>

<ol>
  <li>To the pot of soup, add chicken and stir to combine.</li>
  <li>Use a scoop to drop balls of dough directly into the simmering soup. Place them all around to pot without letting them touch each other.</li>
  <li>Gently shake the pot to let the dumplings settle and not stick together.</li>
  <li>Place a lid on the pot and cook on a low simmer for approximately 28 minutes.</li>
  <li>Remove the lid and check the dumplings for doneness by touching the top of them. The dumplings will be firm to the touch if they are done. If they feel soft and doughy, cook for an additional 2-3 minutes, and check again.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Made 24 dumplings using the #40 scoop.</li>
  <li>The ¾ cup of milk is definitely not enough (always have to use several splashes. Maybe 1 cup is enough?)</li>
</ul>