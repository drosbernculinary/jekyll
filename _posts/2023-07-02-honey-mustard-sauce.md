---
layout: post
title: "Honey Mustard Sauce"
date: 2023-07-02 09:00:59 -0700
categories:
 - sauces-and-condiments
info: "The best homemade honey mustard."
ingredients:
  - 1/4 cup honey
  - 1/4 cup mayonnaise
  - 1/4 cup dijon mustard
  - 1 tbsp white wine vinegar
  - 1/4 tsp cayenne pepper
---

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients and whisk until smooth.</li>
</ol>