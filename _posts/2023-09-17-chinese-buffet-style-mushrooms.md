---
layout: post-carousel
title: "Chinese Buffet Style Mushrooms"
date: 2023-09-17 09:00:59 -0700
categories:
 - sides
 - asian
 - restaurant-recipes
info: "Mushrooms cooked in a garlicky brown sauce."
image: /images/chinese_buffet_style_mushrooms_1.jpg
carousels:
  - images: 
    - image: /images/chinese_buffet_style_mushrooms_1.jpg
    - image: /images/chinese_buffet_style_mushrooms_2.jpg
    - image: /images/chinese_buffet_style_mushrooms_3.jpg
ingredients:
  - 1 large Onion, diced into 1" pieces
  - 24 oz Mushrooms, halved if larger than 1"
  - 2 tbsp Garlic
  - 2 cups Chicken Stock
  - 1 tbsp Cornstarch
  - 2 tbsp Cold Water
  - 1 1/2 tbsp soy sauce
  - 4 tbsp Oyster Sauce
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>On high heat brown the onion.</li>
  <li>Add mushrooms and cook until they have released all their moisture.</li>
  <li>Stir in garlic until it is aromatic (10-30 seconds).</li>
  <li>Add chicken stock and bring to a boil.</li>
  <li>Add soy sauce and oyster sauce.</li>
  <li>Mix cornstarch and cold water in a small bowl to create a cornstarch slurry.</li>
  <li>Next add the cornstarch slurry, mix well, and turn off the heat.</li>
  <li>Taste for salt.</li>
</ol>