---
layout: post
title: "Homemade Sodium Citrate"
date: 2023-12-04 09:00:59 -0700
categories:
 - sauces-and-condiments
info: "Make your own sodium citrate for emulsifying sauces."
image: /images/image_coming_soon.jpg
ingredients:
  - 1/2 tsp Baking Soda
  - 1/5 cup Fresh Lemon Juice (about 50 ml)
ingredients2:
  - 1/3 cup Milk
  - 200 grams Cheese (about 0.85 cups (needs testing))
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Additional Ingredients for Cheese Sauce</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place baking soda in a saucepan.</li>
  <li>Add lemon juice and stir.</li>
  <li>The mixture will fizz. Once the reaction has stopped you are left with sodium citrate.</li>
  <li>To make cheese sauce: add milk and cheese. Cook over medium-low heat, stirring often, until melted.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Once the reaction is done, the mixture will not taste sour anymore because the acidity has been neutralized.</li>
  <li>This amount is capable of emulsifying enough cheese to make a sauce that is too thick. So it has plenty of emulsifying power.</li>
  <li>0.85 cups of cheese was not enough to make a nice cheese sauce. But adding another cup or so was too much and became too thick.</li>
</ul>