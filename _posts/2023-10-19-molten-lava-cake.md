---
layout: post-carousel
title: "Molten Lava Cake"
date: 2023-10-19 09:00:59 -0700
categories:
 - desserts
info: "Cake floating on a dessert sauce."
image: /images/lava_cake_8.jpg
carousels:
  - images: 
    - image: /images/lava_cake_1.jpg
    - image: /images/lava_cake_2.jpg
    - image: /images/lava_cake_3.jpg
    - image: /images/lava_cake_4.jpg
    - image: /images/lava_cake_5.jpg
    - image: /images/lava_cake_6.jpg
    - image: /images/lava_cake_7.jpg
    - image: /images/lava_cake_8.jpg
ingredients:
  - 1 ea - 15.25 oz box Cake Mix (or equivalent)
  - Water, Oil, and Eggs (as per box directions)
  - 2 ea - 3 to 3.5 oz packages Cook & Serve Pudding Mix (not instant)
  - 4 cups Water
  - Powdered Sugar
  - Flavored Syrup (store bought or from below)
ingredients2:
  - 2 cups Powdered Sugar
  - 1/4 cup Flavored liquid (milk, lemon juice, apple juice, etc)
---

<p>{{ info }}</p>

<h2>Lava Cake Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Flavored Glaze Ingredients</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Preheat oven to 325 degrees F.</li>
  <li>Grease a half pan or spray with nonstick spray.</li>
  <li>Prepare cake mix as directed on packaging. Pour cake batter into the half pan.</li>
  <li>Combine pudding mix and water. Mixture will be very thin.</li>
  <li>Gently pour pudding mix over the cake batter using a spoon to deflect the liquid.</li>
  <li>Bake at 325 degrees F for 40-45 minutes or until a toothpick inserted into the center comes out clean.</li>
  <li>Sprinkle a layer of powdered sugar over top.</li>
  <li>Optionally: To make a custom flavored glaze, combine powdered sugar and flavored liquid in a bowl and whisk until smooth.</li>
  <li>Drizzle syrup or custom flavored glaze neatly on top of the powdered sugar.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Custom glaze ideas: apple juice, maple, mango, fruit juices, lemon, lime.</li>
  <li>Cake and pudding ideas: chocolate, strawberry, lemon, pumpkin, gingerbread, caramel apple, mango custard, vanilla, banana, orange dreamsicle, pistachio.</li>
  <li>Instant vanilla pudding was tested successfully in this recipe. Mix the instant pudding and cool water together, pour over the cake, and bake immediately.</li>
</ul>