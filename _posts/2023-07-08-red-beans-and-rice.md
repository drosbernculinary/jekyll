---
layout: post
title: "Red Beans and Rice"
date: 2023-07-08 09:00:59 -0700
categories:
 - mains
 - sides
 - pork
info: "Smokey, spicy, and creamy. This is a hearty bowl of comfort food."
image: /images/red_beans_and_rice.jpg
ingredients:
  - 6 cups Water, cold
  - 2 tbsp Kosher Salt
  - Dry Mexican Red Beans
ingredients2:
  - 4 oz Bacon, 1/4" diced
  - 1 lb Andouille Sausage, sliced into half circles
  - 1 Onion, 1/4" diced
  - 1 Green Pepper, 1/4" diced
  - 2-4 ribs Celery, 1/4" diced
  - Salt (to taste)
  - 1 tsp Black Pepper
  - 2 1/4 tsp Granulated Garlic
  - 1 tsp Paprika
  - 1/2 tsp - 1 tbsp Cayenne
  - 1 tsp Sage
  - 1 tsp Dried Thyme
  - 3 Bay Leaves
  - Soaked Beans, drained and rinsed (from above)
  - 8 cups Water/Stock
  - 1 tsp Apple Cider Vinegar
  - 1 tsp Hot Sauce (Louisiana, Crystal, or Franks)
ingredients3:
  - White Rice
  - Green Onions, thinly sliced
  - More Hot Sauce
---

<p>{{ info }}</p>

<h2>Bean Soaking Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Cooking Ingredients</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Serving Ingredients</h2>

<ul>
    {% for ingredient3 in page.ingredients2 %}
      <li>{{ ingredient3 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Mix water and kosher salt and stir until dissolved, then add dry beans. Soak at room temperature for 8 to 16 hours or overnight. After soaking, drain and Rinse thoroughly.</li>
  <li>Heat bacon in a large pot or Dutch oven, stirring occasionally until beginning to render fat (about 2-3 minutes).</li>
  <li>Add andouille sausage and cook, stirring, until lightly browned and bacon is almost fully rendered (about 5 minutes).</li>
  <li>Add onion, green pepper, celery, and a little salt; cook, stirring frequently until vegetables are softened (about 6-7 minutes).</li>
  <li>Stir in black pepper, granulated garlic, paprika, cayenne, sage, and thyme; cook until fragrant (about 30 seconds).</li>
  <li>Add bay leaves, soaked beans, and water/stock (should be enough to cover beans by about 2 inches).</li>
  <li>Bring to a boil, then reduce to a simmer. Cover and cook until beans are completely tender (about 1 ½ hours or up to 2 ½ hours for older beans.</li>
  <li>Remove lid and continue to cook, stirring occasionally, until liquid has thickened and turned creamy (about 20 minutes). If the pot starts to look dry before the beans turn creamy, add some water, and continue simmering. Repeat as necessary until desired level of creaminess is achieved.</li>
  <li>To increase creaminess of the red beans, remove a cup of beans and place in a small bowl. Mash beans with a fork to create a bean paste. Add bean paste back into the pot of red beans and mix. Repeat, if necessary, until desired level of creaminess is achieved.</li>
  <li>Discard bay leaves. Add apple cider vinegar. Season to taste with hot sauce, salt, and pepper.</li>
  <li>Serve red beans over steamed white rice topped with sliced Green onion and hot sauce, along with a side of cornbread.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>For the best texture and flavor, let cool and refrigerate overnight. Reheat the next day adding a little water to loosen to desired consistency.</li>
  <li>This recipe with 1/2 tsp cayenne is not spicy at all.</li>
</ul>