---
layout: post-carousel
title: "Shallow Poached Fish"
date: 2023-10-03 09:00:59 -0700
categories:
 - mains
 - seafood
info: "A forgiving method to cook any kind of fish, with a sauce that comes together right in the pan."
image: /images/shallow_poached_fish_1.jpg
carousels:
  - images: 
    - image: /images/shallow_poached_fish_1.jpg
    - image: /images/shallow_poached_fish_2.jpg
ingredients:
  - 2 - 4 Fish fillets, approx 6-8 oz each
  - 2 tbsp Butter
  - 1 Shallot, finely diced
  - approx 1/2 cup Fish, Shellfish, or Vegetable stock
  - approx 1/2 cup White Wine or Vermouth
  - approx 1/2 cup Heavy Cream
  - Chopped Parsley (or any other herbs/flavors) (optional)
  - Salt and Pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Season the fish fillets with salt and pepper.</li>
  <li>Begin warming an oven safe pan (choose one with an oven safe lid) over medium heat. Add the butter and shallots; stir occasionally until the butter is melted. (Note: There is no need to cook the shallots at this point, as they will cook plenty during the remainder of cooking time.)</li>
  <li>Add the stock and wine. Increase heat and bring everything to a simmer.</li>
  <li>Carefully place the fish into the simmering liquid. The fish should not be completely submerged. The liquid should only be about 1/2 to 2/3 of the way up the sides.</li>
  <li>Cover the pan with a lid and place into an oven preheated to 350 degrees F. Cook for about 6-8 minutes, or until the fish fillets reach 145 degrees internally. (Optionally: Instead of using the oven, you can cook on the stovetop. Cover, reduce heat to very low, and simmer until the fish is cooked through.)</li>
  <li>Once the fish is cooked, remove them from the pan and set aside. Return the pan to the stove and heat the cooking liquid over medium high heat.</li>
  <li>Cook the liquid until it reduces to a few tablespoons of volume in the bottom of the pan.</li>
  <li>Add the heavy cream and continue cooking, stirring very often, until the cream thickens enough to coat the back of a spoon.</li>
  <li>Remove the pan from the heat, and stir in parsley (or other herbs) if using.</li>
  <li>Taste for seasoning and adjust with salt and pepper if needed.</li>
  <li>Return the fish fillets to the pan and baste with the sauce.</li>
  <li>Plate the fish and spoon additional sauce on top.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Adapted from French Cooking Academy <a href="https://www.thefrenchcookingacademy.com/recipes/shallow-poached-fish-vermouth">here</a> and Arcane Chest <a href="https://www.youtube.com/watch?v=-nqX5qoBd8M">here</a>.</li>
  <li>This technique can be extended to shelfish or poultry, and the flavors can be adjusted as desired.</li>
</ul>