---
layout: post
title: "Cheesecake"
date: 2023-08-07 09:00:59 -0700
categories:
 - desserts
 - sous-vide
info: "Rich and creamy custard filling baked into crunchy graham cracker crust. Knowing the ingredient ratios allows you to make any flavor of cheesecake you can dream up. All perfectly cooked through the magic of sous vide."
image: /images/cheesecake.jpg
ingredients:
  - 1 1/2 cups (about 12 full sheets of graham crackers) Graham Cracker Crumbs
  - 1/4 cup Granulated Sugar
  - 6 tbsp Unsalted Butter, melted
ingredients2:
  - 3 - 8 oz packages (24 oz) Cream Cheese, softened at room temp
  - 1 to 1 1/2 cups Dairy and/or Liquid, at room temp (see note)
  - 1 to 1 1/2 cups Granulated Sugar
  - A pinch of Salt
  - 3 Eggs
---

<p>{{ info }}</p>

<h2>Crust Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Filling Ratio Ingredients</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Crust Instructions</h2>

<ol>
  <li>Place gram crackers in food processor and process into crumbs or in a plastic bag and crush into crumbs.</li>
  <li>Mix graham cracker crumbs and sugar together in a bowl and stir in melted butter. The mixture should be coarse like sand and stick together when you squeeze it.</li>
  <li>Pour mixture into a greased 9-inch spring form pan and push the crust into the bottom and sides until it is no longer crumbly.</li>
  <li>Bake crust at 350 degrees for 10 minutes, then set aside to cool.</li>
  <li>Move on to making your choice of filling.</li>
</ol>

<h2>Filling and Cooking Instructions</h2>

<ol>
  <li>Begin heating the water bath to 176 degrees.</li>
  <li>Once the cheesecake filling is in the springform pan, place a 10-inch pizza tray over the top to cover the cheesecake.</li>
  <li>Place the covered pan into an expandable sous vide bag and vacuum seal while holding the pizza tray in place.</li>
  <li>Place a metal riser into the bottom of the water bath. Place the cheesecake onto the riser and weigh it down so it stays in place, as it will want to float.</li>
  <li>Once the cheesecake is securely positioned, place the lid on the water bath and cook for 3 hours.</li>
  <li>After 3 hours have passed, carefully retrieve the cheesecake from the bath. unbag the cheesecake and remove the pizza tray. Place in the fridge overnight to cool and set up.</li>
  <li>The next day, unlatch the spring and remove the sides of the springform pan. Carefully slide a knife around the outside of the cheesecake to ensure it has separated from the pan, then slide the cheesecake onto a serving tray/pan.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Dairy and/or Liquid, at room temp:</li>
  <ul>
    <li>Dairy: sour cream, ricotta, buttermilk, crème fraiche, mascarpone, goat cheese</li>
    <li>Liquid: citrus juice, fruit puree, molasses (and decrease sugar), canned pumpkin puree, a few ounces of chocolate melted into heavy cream, espresso powder, matcha blended into heavy cream, tahini, peanut or other nut butters, apple or squash butter, dulce de leche, chestnut cream</li>
    <li>For not purely dairy flavored cheesecake go part dairy and part other thing: 1 cup sour cream and 1//2 cup lemon juice, ¾ cup buttermilk and ¾ cup pumpkin puree, equal parts sour cream and goat cheese, or 1 cup dairy and ¼ cup molasses</li>
  </ul>
  <li>Another option is to add dry seasonings like citrus zest, spices, or scraped seeds of a vanilla bean.</li>
  <li>Most of these flavor ideas and ratios sourced from: <a href="https://food52.com/blog/21396-how-to-make-cheesecake-without-a-recipe">Food52.com</a></li>
  <li>To understand how the ingredient ratios affect the cheesecake <a href="https://www.instructables.com/Ultimate-Guide-to-Cheesecake/">Instructables.com</a> did some variation experiments. The control group consisted of 4 packs cream cheese, 1 cup sugar, 1 tsp vanilla, 4 eggs.</li>
  <ul>
    <li>Less eggs: 2 instead of 4: much denser/thicker.</li>
    <li>More eggs: 6 instead of 4: much smoother, creamier, and lighter.</li>
    <li>Sour cream: added 1 cup: more creamy than the one with more eggs, slightly tart taste, slightly less sweet.</li>
    <li>Heavy cream: added ¾ cup: even lighter than the sour cream version, milky taste, smoothest of all versions. Very good.</li>
    <li>Flour: added 2 tbsp: VERY thick, strong flour taste, less sweet, cake-like texture, slightly chalky, the least favorite variant, maybe try 1 tbsp?</li>
    <li>Corn starch: added 2 tbsp: came out just as thick as the one with less eggs but didn’t effect the taste, cake-like texture</li>
  </ul>
  <li>Not sure where to start after all that? For a classic cheesecake with just the right amount of sweetness and a subtle vanilla undertone try: 3 packages of cream cheese, 1 ¼ cup sugar, 1 ½ cup sour cream, 3 eggs, 1 tsp vanilla.</li>
</ul>