---
layout: post
title: "McDonald's Sausage Burrito - Copycat"
date: 2023-08-18 09:00:59 -0700
categories:
 - breakfast
 - restaurant-recipes
info: "Breakfast burritos with the same flavor profile as the iconic chain, but in the confort of your own kitchen."
image: /images/sausage_burrito.jpg
ingredients:
  - 2 tbsp Dried Minced Onion
  - 2 oz Diced Green Chiles
  - 4 oz Canned Diced Tomatoes
  - 1 dozen Eggs
  - 1/2 cup of Whole Milk
  - 1/2 tsp salt
  - 1/2 lb Breakfast Sausage
  - 6 - 10 inch Flour Tortillas
  - 12 slices American Cheese
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Rehydrate the dried minced onions by placing them in a bowl with hot water. Let them sit for a few minutes, while you continue to the next step.</li>
  <li>Drain the tomatoes in with a fine mesh strainer.</li>
  <li>Crack the eggs into a mixing bowl, add milk and salt, and wisk them together until mostly smooth.</li>
  <li>Drain the onions with a fine mesh strainer. Use a spatula to press them against the mesh to get rid of all the water.</li>
  <li>Cook the sausage and set aside to cool.</li>
  <li>In a hot skillet add a little butter, then add the onions and allow them to cook for a couple of minues.</li>
  <li>Add the green chiles and tomatoes, then continue cooking for a couple more minutes.</li>
  <li>Remove the vegetable mixture from the skillet and set aside to cool.</li>
  <li>Cook the eggs in the skillet with a litte more butter over medium heat, stirring constantly until almost fully set but not completely cooked through. Set aside to cool.</li>
  <li>Once the vegetable mixture, sausage, and eggs are no longer releasing steam, place them all in a mixing bowl and mix gently until combined</li>
  <li>Warm the tortillas in the microwave until warm and pliable.</li>
  <li>To assemble, place two slices of cheese in the center of the tortilla. Place 3 #20 scoops of burrito filling on top of the cheese.</li>
  <li>Roll up the burrito and place seam side down into a dry skillet over medium to medium-high heat. Continue rolling burritos and placing them into the skillet as room allows.</li>
  <li>After 30-45 seconds or when the seam side of the burrito has taken on golden brown color and hardened enough to hold its shape, flip the burrito over. Cook another 30-45 seconds on the other side to get that nice golden brown color for presentation.</li>
  <li>Continue until all the burritos have been rolled and sealed.</li>
  <li>The burritos are now ready to enjoy or freeze for later</li>
  <li>To reheat, thaw overnight then microwave for one minute on each side and allow to rest for two minutes.</li>

</ol>

<h2>Notes</h2>

<ul>
  <li>Assembling the burritos with cool ingredients prevents steam from making the burritos soggy. It is even better to cool the ingredients completely in the fridge before assembling.</li>
  <li>Microwaving the burritos gives the tortilla the same texture as McDonald's sausage burritos because they prep the burritos the day before and microwave them to order.</li>
  <li>Aside from the sausage being different, these are probably just a touch of MSG away from being the exact flavor profile of McDonal's sausage burritos.</li>
</ul>