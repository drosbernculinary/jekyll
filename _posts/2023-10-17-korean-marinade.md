---
layout: post-carousel
title: "Korean Marinade"
date: 2023-10-17 09:00:59 -0700
categories:
 - seasonings-and-marinades
 - asian
 - bbq-and-grilling
info: "Bulgogi style Korean BBQ marinade ."
image: /images/korean_marinade_1.jpg
carousels:
  - images: 
    - image: /images/korean_marinade_1.jpg
    - image: /images/korean_marinade_2.jpg
    - image: /images/korean_marinade_3.jpg
    - image: /images/korean_marinade_4.jpg
ingredients:
  - 1 lb Meat (steaks, ribs, chops, etc.)
  - 1/2 cup Pear Puree
  - 1/4 cup Onion Puree
  - 2 tbsp Soy Sauce (Korean Ganjang or any light soy sauce)
  - 2 tbsp Mirin
  - 4 tsp Garlic, chopped
  - 1 tsp Ginger, minced
  - 3/4 tsp Toasted Sesame Oil
  - 2 tbsp Brown Sugar
  - 1  pinch Black Pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients and mix well.</li>
  <li>Marinate meat overnight before cooking.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Remove meat from marinade and pat dry before cooking.</li>
  <li>Adapted from Maangchi's recipe <a href="https://www.maangchi.com/recipe/la-galbi">here</a>.</li>
</ul>