---
layout: post
title:  "New York Style Pizza"
date:   2023-07-01 15:32:14 -0300
categories:
 - mains
 - italian
info: "A large 16-inch pizza with a crispy and slightly chewy crust."
image: /images/new_york_style_pizza.jpg
ingredients:
  - 360 grams Bread Flour, plus more for dusting
  - 7 grams Granulated Sugar
  - 7 grams Kosher Salt (2%)
  - 7 grams King Arthur Dough Enhancer (2%)
  - 240 grams (240 ml) Water, warm (60% (up to 65%))
  - 3.5 grams Instant Yeast (1%)
  - 3.5 grams Oil (1%)
  - New York Style Pizza Sauce
  - Mozzarella Cheese, shredded and placed in freezer for at least 15 minutes
  - Additional Toppings (optional)
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine flour, sugar, salt, and dough enhancer in bowl of food processor. Stir a bit to incorporate.</li>
  <li>In a separate container combine warm water and yeast. Allow the yeast to bloom for about 5 minutes.</li>
  <li>Pour water/yeast and olive oil into the food processor. Run food processor until mixture comes together. Continue processing 30 seconds longer.</li>
</ol>

<h4>Preparation for cooking Same Day</h4>

<ol start="4">
  <li>Place dough into a bowl, cover and let rest for 2 hours.</li>
  <li>Transfer dough ball to a lightly floured surface and knead once or twice by hand and form it into smooth ball.</li>
  <li>Cover the ball again and let rest for another 2 hours.</li>
</ol>

<h4>Preparation For Cooking Another Day</h4>

<ol start="4">
  <li>Transfer dough ball to lightly floured surface and knead once or twice by hand and form it into a smooth ball. It should pass the windowpane test.</li>
  <li>Place dough ball into a lightly oiled container, then lightly oil the top of the dough. Cover tightly with a lid or plastic wrap.Place in refrigerator and allow to rise at least 1 day, and up to 5.</li>
  <li>At least two hours before baking, remove dough from refrigerator and allow to rise at warm room temperature until roughly doubled in volume.</li>
</ol>

<h4>Cooking</h4>

<ol start="7">
  <li>One hour before baking, adjust oven rack with pizza stone to middle position and preheat oven to 500°F (260°C).</li>
  <li>Turn single dough ball out onto lightly floured surface. Gently press out dough into rough 8-inch circle, leaving outer inch higher than the rest. Gently stretch dough by draping over knuckles to form a 14- to 16-inch circle about 1/4-inch thick. Transfer to pizza peel.</li>
  <li>Spread sauce evenly over surface of crust, leaving 1/2- to 1-inch border along edge. Evenly spread cheese over sauce. Add any additional toppings as desired.</li>
  <li>Slide pizza onto baking stone and bake until cheese is melted with some browned spots and crust is golden brown and puffed, 12 to 15 minutes.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Using a food processor ensures that your dough is properly developed without over-oxidizing, which can affect flavor. To scale up, make dough in separate batches in food processor. Do not try to process more than one batch at a time.</li>
  <li>Mostly adapted from Kenji Lopez's New York Style Pizza. Check out his recipe <a href="https://www.youtube.com/watch?v=8qU7nRd9fiU" target="_blank" rel="noopener noreferrer">here</a>.</li>
</ul>