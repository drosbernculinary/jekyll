---
layout: post
title: "Cornbread Dressing"
date: 2023-12-06 09:00:59 -0700
categories:
 - sides
info: "Sweet and buttery, with a hint of sage."
image: /images/cornbread_dressing.jpg
ingredients:
  - 3 cups Chicken Stock
  - 1 cup Onions, 1/4" diced
  - 1 cup Celery, 1/4" diced
  - 2 1/2 tsp Rubbed Sage
  - 1/8 tsp Black Pepper
  - 10 tbsp (5 oz) Melted Butter
  - 3 medium-sized Eggs
  - 1 - 9x13-inch pan Cornbread, baked and cooled (ingredients shown below)
  - 7 1/2 slices White Bread
ingredients2:
  - 2 - 8.5 oz pkgs Jiffy Corn Muffin Mix
  - 2 medium/large Eggs
  - 2/3 cup Milk
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>For the Cornbread</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Heat a small amount of butter or oil in a skillet over medium heat. Add onions and celery, then cook until softened and starting to get some color.</li>
  <li>In a mixing bowl, combine stock, cooked onions and celery, sage, black pepper and melted butter to make the dressing base.</li>
  <li>Allow the dressing base to cool slightly, if necessary, so as to not cook the eggs when added. Add the eggs and mix together.</li>
  <li>Tear each slice of bread into quarters and place in a large mixing bowl.</li>
  <li>Cut the cornbread into 1-inch pieces and gently place into the mixing bowl.</li>
  <li>Pour dressing base over the bread and cornbread. Using gloved hands, mix by fluffing the bottom to the top in a folding motion. Do not overmix. The dressing should still have recognizable chunks of bread and cornbread throughout.</li>
  <li>Pour mixture into a greased half-pan.</li>
  <li>Bake uncovered at 350 degrees F for 30-35 minutesm or until it reaches 165 degrees F internal temperature and is golden brown on the top.</li>
  <li>Stir and repan the dressing to give it a natural appearance, if desired.</li>
</ol>