---
layout: post
title: "Tomatillo Salsa"
date: 2023-07-09 09:00:59 -0700
categories:
 - mexican
 - salsas
info: "Classic salsa verde."
image: /images/tomatillo_salsa.jpg
ingredients:
  - 1 lb Tomatillos, husked and rinsed
  - 4 Jalapenos, destemmed
  - 1/2 Onion
  - 6 cloves Garlic
  - Cilantro
  - Lime Juice
  - Salt
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Cook tomatillos, jalapenos, onion,and garlic on a sheet pan lined will foil until the tomatillos begin to char and most have burst.</li>
  <li>Place tomatillos, jalapenos, onion, and garlic in a blender.</li>
  <li>Add some cilantro, a bit of lime juice, and a little salt.</li>
  <li>Blend.</li>
  <li>Taste for salt, add more as needed, and blend again. Repeat until properly salted.</li>
  <li>Store in refrigerator until ready to serve</li>
</ol>