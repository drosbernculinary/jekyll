---
layout: post
title: "Cornbread"
date: 2023-07-03 09:00:59 -0700
categories:
 - sides
 - breads
info: "Buttermilk is the secret to being moist."
image: /images/cornbread.jpg
ingredients:
  - 1 cup All-Purpose Flour
  - 1 cup Yellow Cornmeal
  - 2/3 cup Granulated Sugar
  - 1 tsp Salt
  - 3 1/2 tsp Baking Powder
  - 1 cup Buttermilk
  - 1 Large Egg
  - 1/3 cup Melted Butter
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Pre-heat the oven to 400 F.</li>
  <li>Place a 12-inch cast iron skillet into the oven to begin heating.</li>
  <li>Whisk together the flour, cornmeal, sugar, salt, and baking powder.</li>
  <li>In a separate bowl, combine the buttermilk, egg, and melted butter.</li>
  <li>Pour the wet ingredients into the dry ingredients, mixing until just combined.</li>
  <li>Butter the inside of the cast iron skillet.</li>
  <li>Pour batter into the skillet and immediately place into the hot oven.</li>
  <li>Bake for 17-20 minutes or until the top of the cornbread is golden brown and a toothpick inserted into the center comes out clean.</li>
</ol>