---
layout: post
title: "Cream Biscuits"
date: 2023-12-11 09:00:59 -0700
categories:
 - breads
 - breakfast
info: "No kneading or cutting in butter required."
image: /images/cream_biscuits.jpg
ingredients:
  - 1 cup (5 oz or 150 g) All-purpose Flour
  - 1 1/2 tsp (about 5 g) Baking Powder
  - 1/4 tsp Kosher Salt (or 1/8 tsp table salt)
  - 5 oz (about 2/3 cup) Heavy Cream
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Preheat oven to 425 degrees F.</li>
  <li>Combine flour, baking powder, and salt.</li>
  <li>Pour in cream and mix into a shaggy dough.</li>
  <li>Dump onto a lightly floured work surface.</li>
  <li>Shape and cut into three pieces.</li>
  <li>Place onto a baking sheet and bake until golden brown, about 15 minutes.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This recipe as written makes three biscuits. Double or triple as needed.</li>
  <li>Flour and cream are equal parts by weight.</li>
  <li>Optionally, brush with cream or butter before baking for more browning.</li>
  <li>Adapted from Kenji Lopez's Five-Ingredient Biscuits and Gravy. Check out his recipe <a href="https://www.youtube.com/watch?v=BoFkDmTm2uc" target="_blank" rel="noopener noreferrer">here</a>.</li>
</ul>