---
layout: post-carousel
title: "Social Dining Dinner Party"
date: 2023-11-02 09:00:59 -0700
categories:
info: "A cross between mongolian bbq and a fondue party. Everyone gets to cook their own bite sized portions of meats and veggies."
image: /images/social_dining_6.jpg
carousels:
  - images: 
    - image: /images/social_dining_1.jpg
    - image: /images/social_dining_2.jpg
    - image: /images/social_dining_3.jpg
    - image: /images/social_dining_4.jpg
    - image: /images/social_dining_5.jpg
    - image: /images/social_dining_6.jpg
ingredients:
  - Marinated Chicken Breast
  - Steak
  - Shrimp
  - Sliced Scallops
  - Pork Steak
  - Ahi Tuna Steak
  - Smoked Sausage, Kielbasa, or Andouille
  - Galbi (beef short ribs)
  - Bulgogi (sliced sirloin, ribeye, or brisket)
  - Pork Belly
ingredients2:
  - Baby or Fingerling Potatoes
  - Onions
  - Mushrooms
  - Zuchinni or Squash
  - Bell Peppers
  - Asparagus
  - Fresh Green Beans
  - Carrots
  - Brussel Sprouts
  - Lettuce Wraps
ingredients3:
  - Horseradish Sauce
  - Ginger Sauce
  - Teriyaki Sauce
  - Sweet Thai Chili Sauce
  - Peanut Sauce
  - Katsu
  - Wasabi Sauce
  - Honey Mustard
  - Ssamjang Sauce
---

<p>{{ info }}</p>

<h2>Protien Options</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Vegetable Options</h2>

<ul>
    {% for ingredient2 in page.ingredients2 %}
      <li>{{ ingredient2 }}</li>
    {% endfor %}
</ul>

<h2>Sauce Options</h2>

<ul>
    {% for ingredient3 in page.ingredients3 %}
      <li>{{ ingredient3 }}</li>
    {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Pre cook and cool any hard vegetables that you are serving (ex. potatoes or carrots). Your guests should be able to cook very item within a couple of minutes.</li>
  <li>Cut all proteins into thin slices (approx. 1/4-inch). Marinate, if necessary.</li>
  <li>Cut all vegetables into bite-sized pieces.</li>
  <li>Pour sauces into serving bowls</li>
  <li>Preheat grill.</li>
  <li>Prepare heated broth.</li>
  <li>Provide each guest a plate and individual skewer forks or tongs.</li>
  <li>Allow guests to grab pieces of protein and veggies and place them onto the grill or into the broth. When it is finished cooking, they can put it on their plate and top with sauces, if desired.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>This is just a template. Choose your own desired combinations of proteins, vegetables, and sauces to serve.</li>
</ul>