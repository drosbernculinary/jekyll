---
layout: post-carousel
title: "Pineapple Marinade"
date: 2023-10-16 09:00:59 -0700
categories:
 - seasonings-and-marinades
 - bbq-and-grilling
info: "Pineapple-soy marinade that is great for grilled meats."
image: /images/pineapple_marinade_1.jpg
carousels:
  - images: 
    - image: /images/pineapple_marinade_1.jpg
    - image: /images/pineapple_marinade_2.jpg
    - image: /images/pineapple_marinade_3.jpg
    - image: /images/pineapple_marinade_4.jpg
ingredients:
  - 1 1/8 cups Canned Pineapple Juice
  - 1/2 cup Soy Sauce
  - 1/2 cup Brown Sugar, packed
  - 1 2/3 tsp Ginger
  - 1/8 tsp White Pepper
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients and mix well.</li>
  <li>Marinate beef, chicken, or pork overnight before cooking.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>The heat generated during the canning process inactivates the bromeline in the pineapple that would otherwise break down the meat too much during a long marination time.</li>
  <li>For grilling: remove meat from marinade and pat dry before cooking.</li>
  <li>For chicken wings: place on a sheet pan with some of the marinade. Bake at 350 degrees F for 40-50 minutes. Flip at leat once while baking to create a glaze.</li>
</ul>