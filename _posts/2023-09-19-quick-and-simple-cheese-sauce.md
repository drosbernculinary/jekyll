---
layout: post-carousel
title: "Quick and Simple Cheese Sauce"
date: 2023-09-19 09:00:59 -0700
categories:
 - sauces-and-condiments
info: "With only three basic ingredients, this cheese sauce comes together in just a few minutes."
image: /images/quick_and_simple_cheese_sauce_1.jpg
carousels:
  - images: 
    - image: /images/quick_and_simple_cheese_sauce_1.jpg
    - image: /images/quick_and_simple_cheese_sauce_2.jpg
    - image: /images/quick_and_simple_cheese_sauce_3.jpg
    - image: /images/quick_and_simple_cheese_sauce_4.jpg
    - image: /images/quick_and_simple_cheese_sauce_5.jpg
ingredients:
  - 1/3 - 1/2 cup Milk
  - 3 slices (1/2 cup or 50 grams) Processed American Cheese
  - 1/2 cup Cheddar Cheese (or any semi-firm cheese), grated and packed
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place milk and cheese in a saucepan and begin heating over medium heat.</li>
  <li>Stir often as the cheese melts and becomes emulsified.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Add a tbsp of butter for added richness, if desired.</li>
  <li>Using 1/3 cup of milk will result in a thicker cheese sauce that is good for macaroni and cheese. Using 1/2 cup of milk will result in a thinner cheese sauce great for dipping or drizzling.</li>
  <li>Adapted from Adam Ragusea's recipe <a href="https://www.youtube.com/watch?v=tSfHVTx1WMk">here</a>.</li>
</ul>