---
layout: post
title: "New York Style Pizza Sauce"
date: 2023-07-02 09:00:59 -0600
categories:
 - sauces-and-condiments
 - italian
info: "Intensely flavored sauce for pizza."
image: /images/pizza_sauce.jpg
ingredients:
  - 28 oz can of tomato puree
  - 1/4 cup olive oil
  - 1 tsp basil
  - 2 tsp sugar
  - 2 tsp salt
  - 1 tsp oregano
  - 4 tsp granulated garlic
  - 1 tsp onion flakes
  - 1 pinch red pepper flakes
  - 1 tsp black pepper
---

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Combine all ingredients in a bowl.</li>
  <li>Regrigerate overnight if possible.</li>
  <li>Use for making pizzas.</li>
</ol>