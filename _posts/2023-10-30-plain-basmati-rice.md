---
layout: post
title: "Plain Basmati Rice"
date: 2023-10-30 09:00:59 -0700
categories:
 - sides
 - asian
info: "Aromatic extra long grain rice."
image: /images/plain_basmati_rice.jpg
ingredients:
  - 1 cup Basmati Rice
  - 1 3/4 cups Water
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Place rice and water in a saucepan.</li>
  <li>Bring to a boil, cover, and reduce heat to low/very low.</li>
  <li>Simmer, covered, for 14 minutes.</li>
  <li>Remove from heat but do not remove the lid. Let it stand, covered, for 10 minutes.</li>
  <li>Fluff rice with a fork and it's ready to serve.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Makes about 3 servings.</li>
</ul>