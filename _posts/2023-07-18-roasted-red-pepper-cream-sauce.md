---
layout: post-carousel
title: "Roasted Red Pepper Cream Sauce"
date: 2023-10-18 09:00:59 -0700
categories:
 - mains
 - sauces-and-condiments
 - italian
info: "A creamy sauce with made from roasted red bell peppers and basil."
image: /images/roasted_red_pepper_sauce_13.jpg
carousels:
  - images: 
    - image: /images/roasted_red_pepper_sauce_1.jpg
    - image: /images/roasted_red_pepper_sauce_2.jpg
    - image: /images/roasted_red_pepper_sauce_3.jpg
    - image: /images/roasted_red_pepper_sauce_4.jpg
    - image: /images/roasted_red_pepper_sauce_5.jpg
    - image: /images/roasted_red_pepper_sauce_6.jpg
    - image: /images/roasted_red_pepper_sauce_7.jpg
    - image: /images/roasted_red_pepper_sauce_8.jpg
    - image: /images/roasted_red_pepper_sauce_9.jpg
    - image: /images/roasted_red_pepper_sauce_10.jpg
    - image: /images/roasted_red_pepper_sauce_11.jpg
    - image: /images/roasted_red_pepper_sauce_12.jpg
    - image: /images/roasted_red_pepper_sauce_13.jpg
ingredients:
  - 1 16 oz jar Roasted Red Peppers
  - 1 Shallot, diced
  - 3 tsp Garlic, chopped
  - 1/2 tsp Dried Basil
  - Black Pepper
  - 1/2 cup Heavy Cream
  - 1 tbsp Butter
  - Lemon Juice, Vinegar, or other acid
  - Salt
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Drain the jar of roasted peppers and place them in a blender. Add just enough stock or water to get them moving and blend until smooth.</li>
  <li>Optional: Pour pepper puree into a fine mesh strainer and use the back of a spoon to push them through. Discard any remaining puree that would not pass through the mesh.</li>
  <li>Optional: Pour pepper puree into a shallow dish. Place the dish into a chamber vacuum sealer and run one cycle to remove all the air that was whipped into the puree while blending.</li>
  <li>Warm a skillet with tall sides over medium heat. Melt some butter and add the shallots. Cook until the shallots begin to soften.</li>
  <li>Stir in garlic and cook until fragrant, about 30 seconds.</li>
  <li>Add pepper puree, dried basil, and black pepper. Continue cooking until the extra liquid has been cooked off and the puree begins to thicken, about 8-10 minutes.</li>
  <li>Add heavy cream and bring to a light simmer.</li>
  <li>Remove sauce from the heat. Stir in 1 tbsp of butter.</li>
  <li>Add a couple of drops of lemon juice or your preferred type of vinegar.</li>
  <li>Taste and adjust for salt</li>
  <li>Stir in approximately 1/3 lb of cooked pasta. Plate and garnish as you like</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>When finishing this sauce, we mount with butter to add richness and mouthfeel. Since the fat coats the palette it can deaden the flavors in the dish. So we add a small amount of acid to cut through the fat and brighten up the sauce. Finally, we adjust the salt to enhance the existing flavors.</li>
</ul>