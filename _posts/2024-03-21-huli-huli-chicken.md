---
layout: post
title: "Huli Huli Chicken"
date: 2024-03-21 09:00:59 -0700
categories:
 - mains
 - seasonings-and-marinades
 - chicken
 - bbq-and-grilling
info: "Huli is Hawaiian for turn. Traditionally cooked rotisserie style, so that the chicken is always turning."
image: /images/huli_huli_chicken.jpg
ingredients:
  - 5 lbs Boneless Skinless Chicken Thighs
  - 1/3 cup Ketchup
  - 1/3 cup Soy Sauce
  - 1/4 cup Brown Sugar
  - 1/4 cup Honey
  - 1/4 cup Shaoxing Cooking Wine
  - 1/2 tsp Sesame Oil
  - 4 tsp Minced Garlic
  - 4 tsp Ginger Puree
  - 1/2 tsp Worcestershire Sauce
  - 1 tsp Sriracha Hot Sauce
  - 1 tsp Lemon Juice
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Create a marinade by combining Ketchup, soy sauce, brown sugar, honey, shaoxing wine, sesame oil, garlic, ginger, worcestershire sauce, sriracha, and lemon juice.</li>
  <li>Optional: Reserve 1/2 cup of marinade for basting when cooking.</li>
  <li>Place chicken pieces into the marinade and allow to marinate for 48 hours.</li>
  <li>Remove chicken from the marinade. Discard used marinade.</li>
  <li>Grill chicken at 400 degrees F until cooked through and slightly charred, basting with reserved marinade if desired.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Adapted from Hawaiian Magazine <a href="https://www.hawaiimagazine.com/how-to-make-hawaii-style-huli-huli-chicken/">here</a>.</li>
</ul>