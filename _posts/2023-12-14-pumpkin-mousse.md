---
layout: post-carousel
title: "Pumpkin Mousse"
date: 2023-12-14 09:00:59 -0700
categories:
 - desserts
info: "A creamy and delicate dessert that captures the essence of autumn with the rich, earthy taste of pumpkin."
image: /images/pumpkin_mousse_2.jpg
carousels:
  - images: 
    - image: /images/pumpkin_mousse_1.jpg
    - image: /images/pumpkin_mousse_2.jpg
    - image: /images/pumpkin_mousse_3.jpg
ingredients:
  - 1 - 3.4 oz package Instant Vanilla Pudding
  - 1 tsp Pumpkin Spice
  - 1/2 tsp Ground Cinnamon, plus more for garnish
  - 1/2 tsp Kosher Salt
  - 1 - 15 oz can Pumpkin Puree
  - 2 tbsp Maple Syrup
  - 1/2 tsp Vanilla Extract
  - 1 cup Whole Milk
  - 2 cups Whipped Cream, plus more for garnish
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>In a large bowl, mix together pudding mix, pumpkin spice, cinnamon, and kosher salt.</li>
  <li>Add pumpkin puree, maple syrup, vanilla, and milk. Beat with a hand mixer until smooth.</li>
  <li>Add whipped cream fold gently with a rubber spatula until smooth.</li>
  <li>Carefully scoop into serving dishes. Top with whipped cream and a sprinkle of cinnamon.</li>
</ol>

<h2>Notes</h2>

<ul>
  <li>Recipe yields 49.5 oz.</li>
  <li>Adapted from Lena Abraham's recipe at <a href="https://www.delish.com/cooking/recipe-ideas/a22130232/easy-pumpkin-mousse-recipe/" target="_blank" rel="noopener noreferrer">Delish.com</a></li>
</ul>