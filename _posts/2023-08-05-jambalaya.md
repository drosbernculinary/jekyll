---
layout: post
title: "Jambalaya"
date: 2023-08-05 09:00:59 -0700
categories:
 - mains
 - pork
 - chicken
 - seafood
info: "The ultimate bowl of comfort. Shrimp, smoked sausage, chicken, in a spicy tomato sauce served over rice."
image: /images/jambalaya.jpg
ingredients:
  - 3 medium green peppers, ½” diced
  - 2 small/medium sweet yellow onions, ½” diced
  - 6 celery ribs, ¼” sliced on a bias
  - 6 cups water
  - 6 cups marinara sauce
  - ¼ cup Cajun seasoning
  - 1 tbsp chicken base
  - 2 tsp granulated garlic
  - 2 tbsp Tabasco Sauce
  - ½ cup flour
  - ½ cup butter (one stick)
  - 8 chicken thighs, cut into ½” strips
  - 4 pkgs Hillshire Farm Smoked Sausage (made with turkey, pork, beef) cut into slices on a bias ⅜” thick
  - 4 lbs. shrimp, raw, deshelled, deveined, tail off
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Cut all vegetables and proteins if not already done so.</li>
  <li>In a large pot, combine water, marinara, Cajun seasoning, chicken base, garlic and Tabasco.</li>
  <li>Sautee green peppers, onions and celery in oil until they begin to soften and add them to the pot.</li>
  <li>Melt the butter in a bowl and then stir in the flour to create a roux.</li>
  <li>Stir the pot to combine all the ingredients and bring to a boil.</li>
  <li>Reduce heat and add roux at the same time. Stir with a wire whisk until sauce becomes thick and smooth.</li>
  <li>Cook the sausage, taking care to brown both sides of each slice. Add sausage to pot as you go and pour all grease that renders from the sausage into the pot too.</li>
  <li>Add chicken to the pot and simmer until fully cooked.</li>
  <li>Next, add shrimp and simmer for approximately 10 minutes or until the shrimp is cooked.</li>
  <li>Separately, use a pot with a lid and cook the rice.</li>
</ol>