---
layout: post
title: "Hometown Buffet Broccoli Bacon Salad"
date: 2023-09-18 09:00:59 -0700
categories:
 - sides
 - salads
 - restaurant-recipes
info: "Crunchy broccoli and crisp bacon tossed in a creamy sauce."
image: /images/hometown_buffet_broccoli_bacon_salad.jpg
ingredients:
  - 3 cups Fresh Broccoli
  - 1/2 cup Cheddar Cheese, shredded
  - 2 tbsp Bacon, cooked and crumbled
  - 1/4 cup Mayonnaise
  - 2 tbsp Granulated Sugar
  - 1 tbsp Buttermilk
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>Chop broccoli into approximately 1/2" pieces and place in a mixing bowl.</li>
  <li>Add cheese and bacon.</li>
  <li>In a separate mixing bowl, stir together mayonnaise, sugar, and buttermilk to create the dressing.</li>
  <li>Add the dressing to the broccoli mixture and mix gently.</li>
</ol>