  const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector(".trigger");
    const navLinks = document.querySelectorAll('.trigger a');
    
    //toggle nav
        burger.addEventListener('click',()=>{
            nav.classList.toggle('nav-active');

        //animate links
        navLinks.forEach((link, index) => {
            if(link.style.animation) {
                link.style.animation = ''
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.15}s`;
            }
        });

        //burger animation
        burger.classList.toggle('toggle');
    });
  }

  navSlide();