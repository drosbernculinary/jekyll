---
layout: post
title: "Vanilla Velvet Cookies"
date: 2023-12-21 09:00:59 -0700
categories:
 - desserts
info: "Not really a sugar cookie as much as a sophisticated cookie with a buttery and vanilla-rich flavor profile."
image: /images/vanilla_velvet_cookies.jpg
ingredients:
  - 1 cup (2 sticks) Salted Butter, softened
  - 1 cup Brown Sugar, packed
  - 1 cup White Granulated Sugar
  - 2 ea Eggs, large
  - 1 tbsp Vanilla Extract
  - 1 tbsp Pure Maple Syrup (or 1 1/2 tsp maple extract) (optional)
  - 3 cups All-purpose Flour
  - 1 tsp Baking Soda
  - 1 tsp Baking Powder
  - Fleur de Sel (or sea salt), for finishing (optional)
---

<p>{{ info }}</p>

<h2>Ingredients</h2>

<ul>
  {% for ingredient in page.ingredients %}
    <li>{{ ingredient }}</li>
  {% endfor %}
</ul>

<h2>Instructions</h2>

<ol>
  <li>In a large bowl, cream butter and sugars. Add eggs, vanilla, and maple syrup (if using). Mix until combined.</li>
  <li>In a separate bowl, whisk together the flour, baking soda, and baking powder.</li>
  <li>Slowly add the dry ingredients into the wet ingredients and mix well.</li>
  <li>Using a #24 disher (3 tbsp or 1.75 oz), portion the dough; placing on a lined sheet pan.</li>
  <li>For cooking immediately:</li>
  <ol type="a">
    <li>Cook at 375 degrees F for 10-11 minutes or until the edges begin to brown.</li>
    <li>Remove from oven and allow the cookies to rest for a couple of minutes before removing from the tray. Sprinkle lightly with finishing salt, if desired.</li>
  </ol>
</ol>
<ol start="5">
  <li>For prepping for a future date:</li>
    <ol type="a">
        <li>Place portioned dough in freezer. Once fully frozen, carefully place each dough ball into a freezer safe bag or vacuum packed until ready to use.</li>
    </ol>
</ol>
<ol start="6">
  <li>For cooking from frozen:</li>
    <ol type="a">
        <li>Place frozen dough balls onto a lined sheet pan.</li>
        <li>Bake at 375 degrees F for about 13 minutes or until the edges begin to brown.</li>
        <li>Remove from oven and allow the cookies to rest for a couple of minutes before removing from the tray. Sprinkle lightly with finishing salt, if desired.</li>
    </ol>
</ol>

<h2>Notes</h2>

<ul>
  <li>Adapted from <a href="https://www.the-girl-who-ate-everything.com/cheat-day-white-chocolate-macadamia-nut/" target="_blank" rel="noopener noreferrer">The Girl Who Ate Everything</a></li>
  <li>Instead of maple, other ideas include: caramel, citrus, nuts/nut butter (like almond butter), cinnamon, nutmeg/cardamom/cloves, coffee, or peach/mango/banana puree.</li>
</ul>